# myStrom-wifi-bulb Web App
A Web App that controls myStrom Wifi bulbs using REST API.


## myStrom API
All myStrom devices offer a REST API (REST = representational State Transfer).

The interface allows you to access/control the device directly from your local network independently of myStrom. That means you don’t need a myStrom account or the myStrom app. With this documentation you can integrate myStrom devices in almost any environment.

### Documentation
Check out the myStrom API documentation [here](https://api.mystrom.ch/?version=latest/).

In our case, we are using the myStrom Bulb. The precise documentation on its usage can be found [here](https://api.mystrom.ch/?version=latest#619e2e50-193b-4393-b271-1d75efb6476a).
## config the ip and mac address in js/app.js
``` javascript
     // first bulb mac and ip
    var macAddress = "6001942C4BAA";
    var ipAdress = "192.168.1.23";
```

## run php local server
``` bash
    php -S localhost:8080 -t .
```